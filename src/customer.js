import React from 'react';
import ReactDOM from 'react-dom/client';

class CustomerForm extends React.Component {
constructor(props) {
    super(props);
    this.state = {
        backendUrl: "http://yagoassessmentbackend-env.eba-p4pdmm6p.eu-west-3.elasticbeanstalk.com",
        firstname: '', lastname: '', email: '', phone: '', address: '', savedCustomer: null, 
        nacebelCode: '', 
        existingCustomerId: '',
        allSavedQuotes: [],
        allCoverAdvices: [],
        allCustomers: [],
        suggestedCovers: null,
        annualRevenue: 123, enterpriseNumber: '0123456789', legalName: 'example SA', naturalPerson: true,
        quotes: null,
        quote: null,
        quoteId: ''
    };
    this.getAllAdvices();
    this.getAllCustomers();
    this.getAllSavedQuotes();
}

handleChangeFirstname = (event) => {
    this.setState({firstname: event.target.value});
}
handleChangeLastname = (event) => {
    this.setState({lastname: event.target.value});
}
handleChangeEmail = (event) => {
    this.setState({email: event.target.value});
}
handleChangeAddress = (event) => {
    this.setState({address: event.target.value});
}
handleChangePhone = (event) => {
    this.setState({phone: event.target.value});
}
handleChangeNacebelcode = (event) => {
    this.setState({nacebelCode: event.target.value});
}
handleChangeAnnualRevenue = (event) => {
    this.setState({annualRevenue: event.target.value});
}
handleChangeEnterpriseNumber = (event) => {
    this.setState({enterpriseNumber: event.target.value});
}
handleChangeLegalName = (event) => {
    this.setState({legalName: event.target.value});
}
handleChangeNaturalPerson = (event) => {
    this.setState({naturalPerson: event.target.value});
}
handleChangeQuoteId = (event) => {
    this.setState({quoteId: event.target.value});
}
handleChangeExistingCustomerId = (event) => {
    this.setState({existingCustomerId: event.target.value});
}

handleSubmitCreateCustomer = (event) => {
    event.preventDefault();
    fetch(this.state.backendUrl + '/customer/', {
        crossDomain:true,
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            firstName: this.state.firstname,
            lastName: this.state.lastname,
            email: this.state.email,
            phoneNumber: this.state.phone,
            address: this.state.address
        }),
    })
    .then(response => {
        if(!response.ok) throw new Error(response.status);
        return response.json();
    })
    .then(data => {
        this.setState({savedCustomer: data})
    });
    this.getAllCustomers();
}
handleSubmitNacebelCode = (event) => {
    event.preventDefault();
    fetch(this.state.backendUrl + '/advice/search?nacebelIds=' + this.state.nacebelCode, {
        crossDomain:true,
        method: 'GET'
    })
    .then(response => response.json())
    .then(data => {
        this.setState({suggestedCovers: data})
    });
}
handleSubmitQuoteRequest = (event) => {
    event.preventDefault();
    fetch(this.state.backendUrl + '/quote-with-advices/', {
        crossDomain:true,
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            annualRevenue: parseInt(this.state.annualRevenue),
            customerId: this.state.savedCustomer.id,
            enterpriseNumber: this.state.enterpriseNumber,
            legalName: this.state.legalName,
            naturalPerson: this.state.naturalPerson,
            nacebelCodes: [this.state.nacebelCode]
        }),
    })
    .then(response => response.json())
    .then(data => {
        this.setState({quotes: data})
    });
}
handleSubmitGetQuoteById = (event) => {
    event.preventDefault();
    fetch(this.state.backendUrl + '/quote/' + this.state.quoteId, {
        crossDomain:true,
        method: 'GET'
    })
    .then(response => response.json())
    .then(data => {
        this.setState({quote: data})
    });
}
handleSubmitGetCustomerById = (event) => {
    event.preventDefault();
    fetch(this.state.backendUrl + '/customer/' + this.state.existingCustomerId, {
        crossDomain:true,
        method: 'GET'
    })
    .then(response => response.json())
    .then(data => {
        this.setState({savedCustomer: data})
    });
}


getAllAdvices = () => {
    fetch(this.state.backendUrl + '/advice/', {
        crossDomain:true,
        method: 'GET'
    })
    .then(response => response.json())
    .then(data => {
        this.setState({allCoverAdvices: data})
    });
}
getAllCustomers = () => {
    fetch(this.state.backendUrl + '/customer/', {
        crossDomain:true,
        method: 'GET'
    })
    .then(response => response.json())
    .then(data => {
        this.setState({allCustomers: data})
    });
}
getAllSavedQuotes = () => {
    fetch(this.state.backendUrl + '/quote', {
        crossDomain:true,
        method: 'GET'
    })
    .then(response => response.json())
    .then(data => {
        this.setState({allSavedQuotes: data})
    });
}


getPremiumsDescription = (premiums) => {

    // those descriptions should come from the backend, be handled in multiple languages and customized for the type of professional
    const coversDescriptions = new Map();
    coversDescriptions.set("AFTER_DELIVERY", "Covers damage arising after delivery of or completion of work (ex: new machines recently installed at the client's office start a fire)");
    coversDescriptions.set("PUBLIC_LIABILITY", "Cover compensation claims for injury or damage (ex: you spill a cup of coffee over a client’s computer equipment)");
    coversDescriptions.set("PROFESSIONAL_INDEMNITY", "Cover compensation claims for a mistake that you make during your work (ex: accidentally forwarded confidential client information to third parties)");
    coversDescriptions.set("ENTRUSTED_OBJECTS", "Objects that don't belong to you, and are entrusted to you. You are obviously liable for any damage to these goods. (ex: you break the super expensive computer that was provided to you as an IT consultant)");
    coversDescriptions.set("LEGAL_EXPENSES", "Also known as legal insurance, is an insurance which facilitates access to law and justice by providing legal advice and covering legal costs of a dispute. (ex: a client asks you for a financial compensation for a mistake you made in your work and you consider it's absolutely not you fault considering the context and you thus want to hire a lawyer to defend you)");
    
    let descriptions = [];
    coversDescriptions.forEach(function(value, key) {
        if (premiums.includes(key)){
            descriptions.push(<p><b>{key}</b>: {value} </p>);
        }
    })
    return (
        <p>
            {descriptions}
        </p>
    );
}


render() {
    return (
        <div>
            {!this.state.savedCustomer &&
            <div>
                <form onSubmit={this.handleSubmitCreateCustomer}>
                    <h3>Create a new Customer</h3> 
                    <label>
                    Firstname:
                    <input type="text" value={this.state.firstname} onChange={this.handleChangeFirstname} />
                    </label>
                    <br/>

                    <label>
                    Lastname:
                    <input type="text" value={this.state.lastname} onChange={this.handleChangeLastname} />
                    </label>
                    <br/>

                    <label>
                    Email:
                    <input type="text" value={this.state.email} onChange={this.handleChangeEmail} />
                    </label>
                    <br/>

                    <label>
                    Address:
                    <input type="text" value={this.state.address} onChange={this.handleChangeAddress} />
                    </label>
                    <br/>

                    <label>
                    Phone Number:
                    <input type="text" value={this.state.phone} onChange={this.handleChangePhone} />
                    </label>
                    <br/>
                    <input type="submit" value="Submit" />
                </form>
                <h3>Or, you can retreive an existing customer by id (see customer list below)</h3>
                <form onSubmit={this.handleSubmitGetCustomerById}>
                    <label>
                        Customer Id:
                        <br/>
                        <input type="text" value={this.state.existingCustomerId} onChange={this.handleChangeExistingCustomerId} />
                    </label>
                    <br/>
                    <input type="submit" value="Submit" />
                 </form>
                <hr/>
            </div>
            }
            { this.state.savedCustomer &&
            <div>
                <h3>Enter a nacebel code to see what are the suggested covers</h3> 
                <p>This code will allow us to give you some advice regarding the covers, deductible formula and coverage ceiling for your profile. You can check the list of supported codes below.</p>
                <form onSubmit={this.handleSubmitNacebelCode}>
                    <label>
                    Nacebel code:
                    <input type="text" value={this.state.nacebelCode} onChange={this.handleChangeNacebelcode} />
                    </label>
                    <br/>
                    <input type="submit" value="Submit" />
                </form>
                {this.state.suggestedCovers && 
                    <div>
                        <h4>Our advice for your case:</h4>
                        <span>You should take the coverage ceiling: <b>{this.state.suggestedCovers[0].coverageCeilingFormula}</b></span><br/>
                        <span>You should take the deductible formula: <b>{this.state.suggestedCovers[0].deductibleFormula}</b></span><br/><br/>
                        <span>You should take the following premiums:</span>
                        {this.getPremiumsDescription(this.state.suggestedCovers[0].premiums)}
                    {/* <span>{JSON.stringify(this.state.suggestedCovers) || []}</span> */}
                    </div>
                }
                <hr/>
                { this.state.suggestedCovers &&
                    <div>
                    <h3>Get a quote</h3> 
                    <p>This quote will automatically be requested using the suggested cover, deductible formula and coverage ceiling suggested before</p> 
                    <form onSubmit={this.handleSubmitQuoteRequest}>
                        <label>
                        Annual Revenue:
                        <input type="text" value={this.state.annualRevenue} onChange={this.handleChangeAnnualRevenue} />
                        </label>
                        <br/>
                        <label>
                        Enterprise Number:
                        <input type="text" value={this.state.enterpriseNumber} onChange={this.handleChangeEnterpriseNumber} />
                        </label>
                        <br/>
                        <label>
                        Legal Name:
                        <input type="text" value={this.state.legalName} onChange={this.handleChangeLegalName} />
                        </label>
                        <br/>
                        <label>
                        Natural person:
                        <input type="checkbox" value={this.state.naturalPerson} onChange={this.handleChangeNaturalPerson} />
                        </label>
                        <br/>
                        <input type="submit" value="Submit" />
                    </form>
                    {this.state.quotes && 
                    <div>
                        <span>Your quote:</span>
                        <table className="table" >
                            <tbody>
                                <tr><td>Quote Id</td><td>{this.state.quotes[0].quoteId}</td></tr>
                                <tr><td>Coverage Ceiling</td><td>{this.state.quotes[0].coverageCeiling}</td></tr>
                                <tr><td>Deductible</td><td>{this.state.quotes[0].deductible}</td></tr>
                                <tr><td>After delivery</td><td>{this.state.quotes[0].afterDelivery}</td></tr>
                                <tr><td>Public liability</td><td>{this.state.quotes[0].publicLiability}</td></tr>
                                <tr><td>Professional indemnity</td><td>{this.state.quotes[0].professionalIndemnity}</td></tr>
                                <tr><td>Entrusted objects</td><td>{this.state.quotes[0].entrustedObjects}</td></tr>
                                <tr><td>Legal expenses</td><td>{this.state.quotes[0].legalExpenses}</td></tr>
                            </tbody>
                        </table>
                    </div>
                    }
                    {/* <span>{JSON.stringify(this.state.quotes) || []}</span> */}
                    <hr/>
                    </div>
                }
            </div>
            }
            {!this.state.savedCustomer &&
            <div>
                <h3>Retreive previous quote</h3> 
                <form onSubmit={this.handleSubmitGetQuoteById}>
                    <label>
                    You can retreive a previous quote and the associated covers by providing its id:
                    <br/>
                    <input type="text" value={this.state.quoteId} onChange={this.handleChangeQuoteId} />
                    </label>
                    <br/>
                    <input type="submit" value="Submit" />
                </form>
                {this.state.quote && 
                    <div>
                    <span>Your quote:</span>
                    <table className="table" >
                        <tbody>
                            <tr><td>Quote Id</td><td>{this.state.quote.quoteId}</td></tr>
                            <tr><td>Coverage Ceiling</td><td>{this.state.quote.coverageCeiling}</td></tr>
                            <tr><td>Deductible</td><td>{this.state.quote.deductible}</td></tr>
                            <tr><td>After delivery</td><td>{this.state.quote.afterDelivery}</td></tr>
                            <tr><td>Public liability</td><td>{this.state.quote.publicLiability}</td></tr>
                            <tr><td>Professional indemnity</td><td>{this.state.quote.professionalIndemnity}</td></tr>
                            <tr><td>Entrusted objects</td><td>{this.state.quote.entrustedObjects}</td></tr>
                            <tr><td>Legal expenses</td><td>{this.state.quote.legalExpenses}</td></tr>
                        </tbody>
                    </table>
                </div>
                }
            </div>
            }
            <br/><br/><br/>
            <hr/><hr/><hr/>
            <span>Backend viewer, this should obviously not be shown in a production case</span>
            <div>
                <h3>Currently supported nacebel codes and associated cover advices</h3>
                <table className="table" >
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Code</th>
                            <th>Suggested Pemiums</th>
                            <th>Suggested coverage ceiling formula</th>
                            <th>Suggested deductible formula</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.allCoverAdvices.map(advice => (
                            <tr key={advice.id}>
                                <td style={{border: '1px solid black'}}>{advice.id}</td>
                                <td style={{border: '1px solid black', backgroundColor: 'cyan'}}>{advice.nacebelId}</td>
                                <td style={{border: '1px solid black'}}>{JSON.stringify(advice.premiums)}</td>
                                <td style={{border: '1px solid black'}}>{advice.coverageCeilingFormula}</td>
                                <td style={{border: '1px solid black'}}>{advice.deductibleFormula}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
            <hr/>
            <div>
                <h3>Currently saved Customers</h3>
                <table className="table" >
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.allCustomers.map(customer => (
                            <tr key={customer.id}>
                                <td style={{border: '1px solid black', backgroundColor: 'cyan'}}>{customer.id}</td>
                                <td style={{border: '1px solid black'}}>{customer.firstName}</td>
                                <td style={{border: '1px solid black'}}>{customer.lastName}</td>
                                <td style={{border: '1px solid black'}}>{customer.email}</td>
                                <td style={{border: '1px solid black'}}>{customer.phoneNumber}</td>
                                <td style={{border: '1px solid black'}}>{customer.address}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
            <hr/>
            <div>
                <h3>Already saved quotes</h3>
                <table className="table" >
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>CustomerId</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.allSavedQuotes.map(savedQuote => (
                            <tr key={savedQuote.id}>
                                <td style={{border: '1px solid black', backgroundColor: 'cyan'}}>{savedQuote.id}</td>
                                <td style={{border: '1px solid black'}}>{savedQuote.customerId}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <hr/>
            </div>
        </div>
    );

    // {
    //     "id": 5,
    //     "customerId": 4,
    //     "coverageCeiling": 800000,
    //     "deductible": 10000,
    //     "quoteId": "seniorTechChallenge984251050",
    //     "afterDelivery": 48.8,
    //     "publicLiability": 183.0,
    //     "professionalIndemnity": 219.6,
    //     "entrustedObjects": 61.0,
    //     "legalExpenses": 73.2,
    //     "coverAdvices": [
    //         {
    //             "id": 2,
    //             "nacebelId": "86210",
    //             "premiums": [
    //                 "LEGAL_EXPENSES"
    //             ],
    //             "coverageCeilingFormula": "large",
    //             "deductibleFormula": "small"
    //         }
    //     ]
    // }
}
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<CustomerForm />);
